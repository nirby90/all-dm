#//lesson #2
v1 <- c (1,2,3,4,5)
v2 <- c(5,6,7,8,9)

mat1 <- rbind(v1,v2) #create matrix by rows
mat2 <- cbind(v1,v2) #create matrix by columns

mat1[v1] #show first line of v1
mat1[,3] #show column 3
mat1[2,4] #show column 4 row 2


#transpose
t(mat2) #show the hofchi
mat1[3] #show the 3 index in the matrix (refer to the matrix as vector)

class(mat1) #the type of the object
typeof(mat1) #data type of the varaible in the object

#random numbers
 
##4 function of random numbers - NORMAL DISTRIBUTION 
dnorm(1,0,1) # (x value,mean,std) - density function
qnorm(0.5,0,1) # (x value,mean,std) - Cumulative distribution
qnorm(0.9999999999999999,mean = 0,sd = 1) # (x value,mean,std) - Cumulative distribution
pnorm(2,0,1) #What is the probability of being smaller than 2
rnorm(10,0,1) #Sampling from a normal distribution

mean(rnorm(10,0,1))

runif(10) #unif distribution

e <- runif(10)*10

#Graphics
data <- matrix(c(1:6,c(10,15,19,26,32,37)),6,2)

plot(data[,1], data[,2]) #(axis x, axis y)
plot(data[,1], data[,2], type = "l") #(axis x, axis y, show line)
plot(data[,1], data[,2], type = "l", ylim=c(0,40)) #(axis x, axis y, show line, axis y limit)


#Histogram
u <- runif(1000,0,100)
hist(u) #genarate histogram

n <- rnorm(1000,0,100)
hist (n)

par(mfrow = c(2,1)) #show graph in 1 column 2 rows
plot(seq(-4,4,0.01),dnorm(seq(-4,4,0.01)), type ="l") #(y,x)
plot(seq(-4,4,0.01),dnorm(seq(-4,4,0.01), mean=0, sd=0.5), type ="l") #(y,x)

#functions

toFar <- function(cel){
  x <-cel*1.8
  x+32
}

toFar(0)

toFar1 <- function(cel){
  x <- cel*1.8 + 32
  return(x)
}

toFar1(20)

derivf <- function(x){ #nigzeret function
  (f(x + 0.01) - f(x))/0.01
}

f <- function(x){
  x^3 + x^2 + x +10
}

derivf(5)

derivf(c(1,3,6,9))

#targil kita
x <- seq(0,20, length = 100)

y <-f(x)
y.derive <- derivf(x)

par(mfrow = c(2,1)) #show graph in 1 column 2 rows
plot(x,y, type = "l")
plot(x,y.derive, type = "l")


#Lists

l <- list(owner = "Jack", sum = 3000)
l[['owner']]
l[[2]]

## $ notaion
l$owner
l$sum

#data frame - similar to database

brands <- c("Ford", "Mazda", "Fiat")
from <- c("US","Japan","Italy")
rank <- c(3,1,2)

cars <- data.frame(brands,from,rank) #connect all vector to table

class(cars)
typeof(cars)

cars$brands
cars$from
cars$rank

typeof(cars$rank)

brands.filter <- cars$brands == "Ford"

cars[brands.filter,]
cars[brands.filter,c(1,3)]
cars[brands.filter,1:2]


summary(cars) #sikum of the database
str(cars)


#Reading data frame from a file
worms <- read.table("worms.txt", header = T)
summary(worms)
worms$Slope
