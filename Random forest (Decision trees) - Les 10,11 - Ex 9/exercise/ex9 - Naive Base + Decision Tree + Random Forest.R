#Repeat the Spam filter Example with Decision Tree model and Random Forest model. 
#Compare the 3 models (The two mentioned and Naive Bayes) using a ROC curve 
#Hint: to plot more than one ROC curve on the save chart add �add = T� to the plot function to all but the first plot 

#------------------------------Naive Bayes Algorithm------------------------------
#Naive Bayes Algorithm
#Spam filter example

spam<-read.csv('spam.csv', stringsAsFactors = FALSE)
#will open the dataSet in a table format 
View(spam)
str(spam)
spam$type <- as.factor(spam$type)

#tm is a text mining package with a bunch of text mining functions
install.packages('tm')
library(tm)
install.packages('e1071')
library(e1071)
#genrating a word cloud of spam and not spam 
install.packages('wordcloud')
library(wordcloud)
#creating confision matrix
install.packages('SDMTools')
library(SDMTools)
install.packages('RColorBrewer')
library(RColorBrewer)
install.packages('NLP')
library('NLP')

#Generate DTM
#1. Build CORPUS
spam_corpus <- Corpus(VectorSource(spam$text))
spam_corpus[[1]][[1]]
spam_corpus[[1]][[2]]
spam_corpus[[10]][[1]]
spam_corpus[[2]][[2]]

#2. Clean the text
#���� ����� �����
clean_corpus <- tm_map(spam_corpus,removePunctuation)
#������ ������ ������
clean_corpus <- tm_map(clean_corpus, content_transformer(tolower))
#���� ����� ������ ������
clean_corpus <- tm_map(clean_corpus, removeWords, stopwords())
#���� ������ ������
clean_corpus <- tm_map(clean_corpus, stripWhitespace)

#3. Generate DTM
dtm <- DocumentTermMatrix(clean_corpus)

dim(dtm)

clean_corpus[[1]][[1]]

#4. Reduce DTM columns
#remove infrequent words
frequent_dtm <- DocumentTermMatrix(clean_corpus, list(dictionary = findFreqTerms(dtm,10)))
dim(frequent_dtm)

#5. Data visualization
pal <- brewer.pal(9, 'Dark2')

wordcloud(clean_corpus, min.freq = 5, random.order = FALSE, colors = pal)
#spam
wordcloud(clean_corpus[spam$type == 'spam'], min.freq = 5, random.order = FALSE, colors = pal)
#ham
wordcloud(clean_corpus[spam$type == 'ham'], min.freq = 5, random.order = FALSE, colors = pal)

#6. Spliting into training and test set
split <- runif(500)
split <- split > 0.3

#dividing raw data
train_raw <- spam[split,]
test_raw <- spam[!split,]

#dviding clean corpus
train_corpus <- clean_corpus[split]
test_corpus <- clean_corpus[!split]

#dividing dtm
train_dtm <- frequent_dtm[split,]
test_dtm <- frequent_dtm[!split,]

#convert the DTM into yes/no
conv_yesno <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x,levels = c(1,0), labels = c('Yes', 'No'))
}

train <- apply(train_dtm, MARGIN = 1:2,conv_yesno)
test <- apply(test_dtm, MARGIN = 1:2,conv_yesno)

#converting into a data frame
df_train <- as.data.frame(train)
df_test <- as.data.frame(test)

#add the type column
df_train$type <- train_raw$type
df_test$type <- test_raw$type

dim(df_train)

df_train[,58]

#7. Generating the model using naive bayes
model <- naiveBayes(df_train[,-58], df_train$type)

prediction.nb <- predict(model,df_test[,-58])

prediction.prob.nb <- predict(model,df_test[,-58], type = 'raw')

prediction.prob.nb[,'spam']

#building roc chart
install.packages('pROC')
library(pROC)
rocCurve.nb <- roc(test_raw$type, prediction.prob.nb[,'spam'], levels = c("spam","ham"))
plot(rocCurve.nb, col="yellow", main='ROC chart')

#------------------------------Decision Tree Model------------------------------
install.packages('rpart')
library(rpart)

tree <- rpart(type ~ ., method = 'class', data = df_train)

install.packages('rpart.plot')
library(rpart.plot)

prp(tree)

predicted.dt <- predict(tree, df_test[,-58])

predicted.prob.dt <- as.data.frame(predicted.dt)

predicted.prob.dt[,'spam']

rocCurve.dt <- roc(test_raw$type, predicted.dt[,'spam'], levels = c("spam","ham"))

plot(rocCurve.dt, col = "red", main = "Roc Chart")


#------------------------------Random Forest Model------------------------------
install.packages('randomForest')
library(randomForest)

str(df_train)

rf.model <- randomForest(type ~ ., data = df_train)

print(rf.model)

predicted.rf <- predict(rf.model, df_test)

predicted.prob.rf <- predict(rf.model,df_test, type = 'prob')

predicted.prob.rf[,'spam']

rocCurve.rf <- roc(test_raw$type, predicted.prob.rf[,'spam'], levels = c("spam","ham"))

plot(rocCurve.rf, col = "blue", main = "Roc Chart")


#------------------------------Main ROC Chart------------------------------
plot(rocCurve.nb , col="yellow", main='ROC chart')
plot(rocCurve.dt, col = 1, lty = 2,add = TRUE)
plot(rocCurve.rf, col = 4, lty = 3, add = TRUE)











