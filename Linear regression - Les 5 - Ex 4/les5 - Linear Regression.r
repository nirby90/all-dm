# -- Linear regression bicycle --

bike <- read.csv("train.csv")
str(bike)

# Extracting the hour from datetime
#This is feature engineering

#First we change it to a character
bike$datetime <- as.character(bike$datetime)

#Then we split date and time by blank
bike$date <- sapply(strsplit(bike$datetime,' '), "[", 1)
bike$date <-as.Date(bike$date)

bike$time <- sapply(strsplit(bike$datetime,' '), "[", 2)

bike$hour <- sapply(strsplit(bike$time,':'), "[", 1)
bike$hour <- as.numeric(bike$hour)

bike$time <- NULL

bike$season <- factor(bike$season , levels = c(1,2,3,4), labels = c('spring', 'summer', 'fall', 'winter'))
levels(bike$season)

library(ggplot2)

#how temperture affects count
pl1 <- ggplot(bike, aes(x = temp, y = count))
pl1 <- pl1 + geom_point(aes(color = temp), alpha = 0.3)
pl1 <- pl1 + ggtitle('A Chart Line of temperature affects the count')

#how date affects count
pl2 <- ggplot(bike, aes(date,count))
pl2 <- pl2 + geom_point(alpha = 0.4, aes(color=temp))
pl2 <- pl2 + scale_color_continuous(low = 'blue', high = 'red')

#how season affects count
pl3 <- ggplot(bike, aes(season, count)) + geom_boxplot(aes(color = season))

#how hour affects count on weekdays
pl4 <- ggplot(bike[bike$workingday != 1,], aes(hour, count))
pl4 <- pl4 + geom_point(position = position_jitter(w = 0.5, h = 0), aes(color = temp))

#how hour affects count on weekdays
pl5 <- ggplot(bike[bike$workingday == 1,], aes(hour, count))
pl5 <- pl5 + geom_point(position = position_jitter(w = 0.5, h = 0), aes(color = temp))

bike$datetime <- NULL

splitDate <- as.Date('2012-05-01')

dateFilter <- bike$date <= splitDate

bike.train <- bike[dateFilter,]
bike.test <- bike[!dateFilter,]

model <- lm(count ~ . -atemp -casual -registered -date, bike.train)

summary(model)

predicted.train <- predict(model,bike.train)
predicted.test <- predict(model,bike.test)

MSE.train = mean((bike.train$count-predicted.train)**2)
MSE.test = mean((bike.test$count-predicted.test)**2)

RMSE.train <- MSE.train**0.5
RMSE.test <- MSE.test**0.5
