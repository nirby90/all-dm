#----------HW5----------

#1. Read the file 
adult <- read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv'))
str(adult)

#2. Reduce the number of levels for country
levels(adult$country)
table(adult$country)

Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
            "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
            "Portugal")


Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
          "Taiwan", "Thailand" ,  "Laos", "India",  "Vietnam"    )


North.america <- c("United-States" ,  "Canada", "Puerto-Rico"  ) 

Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
                             "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )
Other <- c("South")

group_country <- function(ctry){
  if(ctry %in%  Europe) 
  {return("Europe")} 
  else if (ctry %in% Asia)
  {return("Asia")}
  else if (ctry %in% North.america)
  {return("North.america")}
  else if (ctry %in% Latin.and.south.america)
  {return("Latin.and.south.america")}
  else 
  {return('Other')} 
}

adult$country <- sapply(adult$country,group_country)

table(adult$country)

adult$country <- factor(adult$country)

#3. Reduce the number of levels for ‘marital’  ( 3 levels ) and type_employer (5 levels)
#--martial--
levels(adult$marital)
table(adult$marital)

group_marital <- function(mar){
  mar <- as.character(mar)
  if(mar == 'Separated' | mar == 'Divorced' | mar == 'Widowed'){
    return('Not-married')
  }else if(mar == 'Never-married'){
    return(mar)
  } else {
    return('Married')
  }
}

adult$marital <- sapply(adult$marital ,group_marital )

table(adult$marital)

adult$marital  <- factor(adult$marital)

#--type_employer--
levels(adult$type_employer)
table(adult$type_employer)

unemp <- function(job){
  job <- as.character(job)
  if(job == 'Never-worked' | job == 'Without-pay'){
    return('Unemployed')
  }else{
    return(job)
  }
}

adult$type_employer <- sapply(adult$type_employer,unemp)

table(adult$type_employer)

str(adult)

group_emp <- function(job){
  if(job == 'Local-gov' | job == 'State-gov' | job == 'Federal-gov' ){
    return('Gov')
  }else if(job == 'Self-emp-inc' | job == 'Self-emp-not-inc'){
    return('Self-emp')
  } else {
    return(job)
  }
}

adult$type_employer <- sapply(adult$type_employer,group_emp)

adult$type_employer <- factor(adult$type_employer)

table(adult$type_employer)

#--check that everything O.K.--
str(adult)

#4. Remove all records with missing data 
adult[adult=='?'] <- NA

table(adult$type_employer)

missmap(adult)

adult <- na.omit(adult)

#5. Remove the index column from the dataset 
library(dplyr)

adult <- select(adult, -X)

#6. Use histograms to review the effect of the variables on the target attribute 
levels(adult$income)

ggplot(adult, aes(age)) + geom_histogram(aes(fill = income),color= 'black', binwidth = 1)

adult <- rename(adult, region = country)

str(adult)

#7. Split the dataset to train and test 
install.packages('caTools')
library(caTools)

set.seed(101)
sample <- sample.split(adult$income, SplitRatio = 0.7)

adult.train <- subset(adult, sample ==T)
adult.test <- subset(adult, sample ==F)

#8. Compute the logistic regression model 
model.income <- glm(income ~ ., family = binomial(link = 'logit'), data = adult.train)
summary(model.income)

#9. Use the test set to find the error rate 
step.model.income <- step(model.income)

summary(step.model.income)

predicted.test.income <- predict(model.income, newdata = adult.test, type = 'response')

summary(predicted.test.income)

misClassError <- mean(predict.values != adult.clean.test$income)
#10. Submit the code and screenshots of charts






